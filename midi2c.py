#### config ############
input_base_folder  = 'C:\\evolaris\\CloudStation\\98-Projekte\\054-STM32EB\\STM32EB-V1.0\\07-Software-Implementation\\SW_STM32EB_fresh\\src\\core\\mozart\\midis'
output_base_folder = 'C:\\evolaris\\CloudStation\\98-Projekte\\054-STM32EB\\STM32EB-V1.0\\07-Software-Implementation\\SW_STM32EB_fresh\\src\\core\\mozart\\songs'
subfolder = 'sound effects'

#### code ##############
print('conversion starting ~~~~~~~~~~~~~~~~~~~~~~')
import mido
import os

tones = [
    'C',
    'Cs',
    'D',
    'Ds',
    'E',
    'F',
    'Fs',
    'G',
    'Gs',
    'A',
    'As',
    'B',
]

def pitch2tone(pitch):
    if pitch <= 127:
        pitch_transposed_to_base = pitch % 12
        return tones[pitch_transposed_to_base]
    else:
        print('too high tone!')
        return 0

def pitch2octave(pitch):
    if pitch <= 127:
        return pitch // 12
    else:
        print('too high tone!')
        return 0

def remove_duplicate_tracks(filename):
    cv1 = mido.MidiFile(filename, clip=True)
    message_numbers = []
    duplicates = []

    for track in cv1.tracks:
        if len(track) in message_numbers:
            duplicates.append(track)
        else:
            message_numbers.append(len(track))
    for track in duplicates:
        cv1.tracks.remove(track)
    cv1.save(filename)

def select_pitch_to_play(currently_played_pitches):
    return max(currently_played_pitches) 

input_folder = input_base_folder + '\\' + subfolder
output_folder = output_base_folder + '\\' + subfolder
f = []
for (_, _, filenames) in os.walk(input_folder):
    f.extend(filenames)
    break

for midi_file in f:
    print('converting file "' + midi_file + '"')
    midi_path = input_folder + '\\' + midi_file
    remove_duplicate_tracks(midi_path)

    mid = mido.MidiFile(midi_path, clip=True)

    us_per_tick = None
    merged_track = mido.merge_tracks(mid.tracks)
    currently_played_pitches = []
    c_command_list = ''
    time_for_meta_messages = 0

    for message in merged_track:
        if not us_per_tick is None:
            time_in_ticks = message.time
            duration_ms = round((time_in_ticks * us_per_tick) / 1000)
        else:
            duration_ms = 0

        if message.type == 'note_on' or message.type == 'note_off':
            pitch = message.note
            note_on = message.velocity != 0 and message.type != 'note_off'

            duration_with_meta_ms = duration_ms + time_for_meta_messages
            time_for_meta_messages = 0
            if duration_with_meta_ms != 0:
                if currently_played_pitches:
                    pitch_to_play = select_pitch_to_play(currently_played_pitches)
                    tone = pitch2tone(pitch_to_play)
                    octave = pitch2octave(pitch_to_play)
                    line_to_add = 'mozart_play_tone_and_time(t_' + tone + ', ' + str(octave) + ', ' + str(duration_with_meta_ms) + ');\n'
                    
                else:
                    line_to_add =  'mozart_pause_time(' + str(duration_with_meta_ms) + ');\n'
                if len(c_command_list.split('\n')) < 1500:
                    c_command_list += line_to_add

            if note_on:
                currently_played_pitches.append(pitch)
            else:
                if pitch in currently_played_pitches:
                    currently_played_pitches.remove(pitch)
                else:
                    print('oh-oh')
        else:
            time_for_meta_messages += duration_ms
            if message.type == 'set_tempo':
                us_per_beat = message.tempo
                us_per_tick = us_per_beat / mid.ticks_per_beat

    if currently_played_pitches:
        print('remaining pitches!!')
        print(currently_played_pitches)
    if c_command_list:
        basename_with_extension = os.path.basename(midi_file)
        basename = os.path.splitext(basename_with_extension)[0]
        text_file = open(output_folder + '\\' + basename + '.h', 'w')
        n = text_file.write(c_command_list)
        text_file.close()
    
print('conversion finished ~~~~~~~~~~~~~~~~~~~~~~')


